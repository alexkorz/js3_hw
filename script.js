// TASK 1

let age = +prompt('Please enter your age:');

if(isNaN(age) || age <= 0 || age === undefined){
  alert('Please enter a valid age');
} else if (age >= 1 && age < 12){
  alert('You are a child');
} else if (age >= 12 && age < 18) {
  alert('You are a teenager');
} else {
  alert('You are an adult');
}

// TASK 2

let month = prompt('Будь ласка, напишіть місяць: ');

switch (month.toLowerCase()) {
  case 'січень' || 'березень' || 'травень' || 'липень' || 'серпень' || 'жовтень' || 'грудень':
    console.log(`There are 31 days in ${month}`);
    break;
  case 'лютий':
    console.log(`There are 28 or 29 days in ${month}`);
    break;
  default:
    console.log(`There are 30 days in ${month}`);
}